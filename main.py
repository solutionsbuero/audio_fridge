import RPi.GPIO as GPIO
import simpleaudio as sa
from time import sleep
import os
import subprocess


def list_files():
	global flist, fnumber
	print("reading folder...")
	fnames = os.listdir("audio")
	fnumber = len(fnames)
	for i in fnames:
		flist.append(sa.WaveObject.from_wave_file("audio/%s" % i))
	print("found %d files: %s" % (fnumber, ', '.join(fnames)))


def play_file(objnr):
	print("starting playback")
	play_obj = flist[objnr].play()
	play_obj.wait_done()
	print("playback done")


def check_switch():
	global previous_state, current_file
	current_state = GPIO.input(sensor_pin)
	if current_state == False and previous_state == True:
		sleep(0.5)
	if current_state == True and previous_state == False:
		print("Event triggered, jump to playback function")
		play_file(current_file)
		current_file = (current_file + 1) % fnumber
		sleep(0.5)
	previous_state = current_state


def check_shutdown():
	sd_state = GPIO.input(shutdown_pin)
	if sd_state == False:
		print("Shutting down in 1s...")
		sleep(1)
		subprocess.Popen("sudo shutdown -h now", shell=True)


'''
Global Scope, Setup
'''
# Physical pin 40 -> GND 39
sensor_pin = 14
# shutdown_pin = 5
GPIO.setmode(GPIO.BCM)
GPIO.setup(sensor_pin, GPIO.IN)
# GPIO.setup(shutdown_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
sleep(1)
print("Hardware setup done")
flist = []
fnumber = 0
current_file = 0
previous_state = True

list_files()

while True:
	check_switch()
	check_shutdown()
